package fr.esipe.ing2;

import org.apache.commons.math3.primes.Primes;

public class Prime {
    private IService service;


    public Prime(IService service) {
        this.service = service;
    }

    public boolean isPrime() {
        return Primes.isPrime(this.service.Call());
    }
}
